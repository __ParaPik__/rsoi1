from auth_app import app
from flask import redirect
from auth_app.slack import SlackAuth
from auth_app.slack import SlackAPI
from flask import request
from flask import make_response
from flask import render_template


__token_key_gen = 0
__token_map = {}


@app.route("/")
def index():
    return make_response(render_template("index.html"))


@app.route("/auth")
def auth():
    resp = SlackAuth.initiate_login(redir_route="/auth_code")
    app.logger.debug("Auth redirect response: " + resp)
    return redirect(resp)


@app.route("/auth_code")
def auth_answer():
    global __token_key_gen

    code = request.args.get("code")
    app.logger.debug("Code " + code)
    token = SlackAuth.get_token(code, route="/auth_code")
    key = str(__token_key_gen)
    __token_map[key] = token
    resp = make_response(redirect("/profile"))
    resp.set_cookie("token", key)
    __token_key_gen += 1
    app.logger.debug("Making redirect to /profile with token=" + key)
    return resp


@app.route("/profile")
def profile():
    return make_response(render_template("profile.html"))


@app.route("/profile/details")
def profile_details():
    token_index = request.headers["Authority"]
    api = SlackAPI(token=__token_map[token_index])
    resp = render_template("profile_info.html", profile=api.get_profile())
    app.logger.debug("Profile details answer for token=" + token_index + ": " + resp)
    return resp


@app.route("/channels")
def get_channels():
    token_index = request.headers["Authority"]
    api = SlackAPI(token=__token_map[token_index])
    app.logger.debug(api.get_channel_list())
    resp = render_template("channels.html", list=api.get_channel_list())
    app.logger.debug("Channels answer for token=" + token_index + ": " + resp)
    return resp
