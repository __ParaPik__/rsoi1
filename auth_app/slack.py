import json
from urllib import request as urllib2
from datetime import datetime


class SlackAuth:

    __client_id__ = "87523858100.87528167248"
    __client_secret__ = "676aa06fc932c3158422ad5ff4cd15b7"
    __first_phase_url__ = "https://slack.com/oauth/authorize"
    __second_phase_url__ = "https://slack.com/api/oauth.access"

    @staticmethod
    def initiate_login(redir_route):
        scope = "users.profile:read channels:read"
        redirect_uri = SlackAuth._get_redirect_uri(redir_route)
        return SlackAuth.__first_phase_url__ + "?" + "client_id=" + SlackAuth.__client_id__ \
               + "&scope=" + scope + "&redirect_uri=" + redirect_uri

    @staticmethod
    def _get_redirect_uri(address):
        return "http://127.0.0.1:5000" + address

    @staticmethod
    def get_token(code, route):
        req = urllib2.Request(SlackAuth.__second_phase_url__
                              + "?client_id=" + SlackAuth.__client_id__
                              + "&client_secret=" + SlackAuth.__client_secret__
                              + "&code=" + code
                              + "&redirect_uri=" + SlackAuth._get_redirect_uri(route))

        resp = urllib2.urlopen(req)
        data = resp.read().decode('utf-8')
        content = json.loads(data)
        return content["access_token"]


class SlackProfile:

    def __init__(self, json_data):
        profile = json_data["profile"]
        self.email = profile["email"]
        self.name = profile["real_name"]
        self.image = profile["image_192"]


class SlackChannel:

    def __init__(self, json_data):
        purpose = json_data["purpose"]
        self.purpose = purpose["value"]
        self.date = datetime.fromtimestamp(json_data["created"])


class SlackAPI:

    __api_url__ = "https://slack.com/api"

    def __init__(self, token):
        self.token = token

    def get_profile(self):
        url = SlackAPI.__api_url__ + "/users.profile.get"
        url += "?token=" + self.token
        req = urllib2.Request(url)
        resp = urllib2.urlopen(req)
        data = resp.read().decode('utf-8')
        content = json.loads(data)
        return SlackProfile(content)

    def get_channel_list(self):
        url = SlackAPI.__api_url__ + "/channels.list"
        url += "?token=" + self.token
        req = urllib2.Request(url)
        resp = urllib2.urlopen(req)
        data = resp.read().decode('utf-8')
        content = json.loads(data)
        channels = content["channels"]
        result = []
        for channel in channels:
            result.append(SlackChannel(channel))
        return result
