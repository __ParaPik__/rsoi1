$(document).ready(function() {
    var width = $(window).width();
    var height = $(window).height();

    var elem = $("a");
    elem.css("left", (width - elem.width()) / 2).css("top", (height - elem.height()) / 2);

    elem.click(function() {
        window.location.href = "/auth";
    });
});
