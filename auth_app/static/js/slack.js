function makeRequest(url, success) {
    $.ajax({
            type:"GET",
            beforeSend: function (request) {
                request.setRequestHeader("Authority", Cookies.get("token"));
            },
            url: url,
            success: success
        });
}


$(document).ready(function() {
    $("#details").click(function(){
        makeRequest("/profile/details", function(data) {
            $("#profile_info").html(data);
        });
    });

    $("#channels").click(function(){
        makeRequest("/channels", function(data) {
            $("#channels_list").html(data);
        });
    });
});